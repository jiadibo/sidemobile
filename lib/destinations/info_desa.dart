import 'package:flutter/material.dart';
import 'package:card_settings/card_settings.dart';
import 'package:simple_slider/simple_slider.dart';

class Infodesa extends StatefulWidget {
  static const String tag = 'info_desa_activity';
  @override
  _InfodesaState createState() => _InfodesaState();
}

class _InfodesaState extends State<Infodesa> {
  final _imageAssets = [
    "https://spectrumproperties.co.ug/wp-content/uploads/2018/11/Bugos1-1024x713.jpeg",
    "https://spectrumproperties.co.ug/wp-content/uploads/2018/11/Innovation-is-the-central-issue-in-economic-prosperity.-1024x576.png",
    "https://spectrumproperties.co.ug/wp-content/uploads/2018/10/DSC_0148.jpg"
  ];

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final GlobalKey<FormState> _namaDesa = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Identitas Desa'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {},
          ),
        ],
        leading: IconButton(
          icon: Icon(Icons.refresh),
          onPressed: () {},
        ),
      ),
      body: new Container(
          key: _formKey,
          child: new ListView(
            children: <Widget>[
              ImageSliderWidget(
                imageHeight: 50,
                imageUrls: _imageAssets,
                imageBorderRadius: BorderRadius.circular(4.0),
              ),
              CardSettings(
                children: <Widget>[
                  CardSettingsHeader(label: 'Identitas Desa'),
                  CardSettingsText(
                    // key: _nameKey,
                    label: 'Nama Lengkap',
                    hintText: 'Masukkan nama lengkap',
                    // initialValue: _formModelKpnddkn.nama,
                    // autovalidate: _autoValid,
                    validator: (value) {
                      if (value == null || value.isEmpty)
                        return 'Name type is required.';
                      return null;
                    },
                    // onSaved: (value) => _formModelKpnddkn.nama = value,
                    // onChanged: (value) => _showSnackBar('Name', value),
                  ),
                  _buildCardSettingsTextDesa(),
                  _buildCardSettingsTextDesa(),
                  _buildCardSettingsTextDesa(),
                  _buildCardSettingsTextDesa(),
                  _buildCardSettingsTextDesa(),
                ],
              ),
            ],
          )),
    );
  }

  CardSettingsText _buildCardSettingsTextDesa() {
    return CardSettingsText(
      // key: _desaKey,
      label: 'Desa',
      hintText: 'Nama Desa',
      // initialValue: _formModelPmbgnn.desa,
      // autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Name type is required.';
        return null;
      },
      // onSaved: (value) => _formModelPmbgnn.desa = value,
      // onChanged: (value) => _showSnackBar('Desa', value),
    );
  }
}

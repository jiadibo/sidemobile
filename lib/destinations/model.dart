import 'package:flutter/material.dart';

class FormModelPenduduk {
  String nama = 'Miftahul Jihad';
  String tempat_lahir = 'Mataram';
  String alamat = 'Kekalik';
  DateTime tgl_lahir = DateTime(2010, 10, 10, 20, 30);

  String jk = "-- Pilih --";
  String agama = "-- Agama --";
  String pend_terakhir = "-- Pendidikan --";
  String pekerjaan = "-- Pekerjaan --";
  String status = '-- Status --';
  String stts_keluarga = "-- Status Keluarga --";
  String gol_darah = "-- Gol. Darah --";

  //==========
  // void _pilihJk(String value) {
  //   setState(() {
  //     _jk = value;
  //   });
  // }

  List<String> hobbies = <String>[
    'flying',
    'singing',
    'exploring',
    'hiding',
    'coloring'
  ];
}

class FormModelPembangunan {
  String desa = '';
  DateTime tahun = DateTime(2010);
  String bidang = '';
  String sub_bidang = '';
  String j_kegiatan = '';
  String lok_kegiatan = '';
  String sasaran = '';
  DateTime tahun_mulai = DateTime(2010);
  DateTime tahun_berakhir = DateTime(2010);
  double prakiraan_biaya=0.0;
  String sumber_biaya = '';
  String keterangan = '';
  String pj_penyusun = '';

  String indikasi = '';
  String program_bidang = '';
  String prakiraan_sasaran = '';
  String keterangan_indikasi = '';
}

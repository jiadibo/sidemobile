import 'package:flutter/material.dart';
import 'model.dart';
import 'package:card_settings/card_settings.dart';
import 'dart:async';

import 'result.dart';

class FormPenduduk extends StatefulWidget {
  static const String tag = 'form_penduduk';
  @override
  _FormPendudukState createState() => _FormPendudukState();
}

class _FormPendudukState extends State<FormPenduduk> {
  final _formModelKpnddkn = FormModelPenduduk();

  bool _autoValid = false;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<FormState> _nameKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _lahirKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _alamatKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _dateKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _jkKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _agamaKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _pendterakhirKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _pekerjaanKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _statusKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _statuskeluargaKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _goldarKey = GlobalKey<FormState>();


  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: appBar(),
      body: Form(
        key: _formKey,
        child: (orientation == Orientation.portrait)
            ? _buildPortraitLayout()
            : _buildLandscapeLayout(),
      ),
    );
  }

  AppBar appBar() => AppBar(
        title: Text("Form Penduduk"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {},
          ),
        ],
        leading: IconButton(
          icon: Icon(Icons.refresh),
          onPressed: () {},
        ),
      );

  //CARD SETTING FOR EACH LAYOUT

  CardSettings _buildPortraitLayout() {
    return CardSettings(
      children: <Widget>[
        CardSettingsHeader(label: 'Person'),
        _buildCardSettingsText_Name(),
        _buildCardSettingsText_Tlahir(),
        _buildCardSettingsDatePicker(),
        _buildCardSettingsText_Alamat(),
        _buildCardSettingsList_Jk(),
        _buildCardSettingsList_Agama(),
        _buildCardSettingsList_PendTerakhir(),
        _buildCardSettingsList_Pekerjaan(),
        _buildCardSettingsList_Status(),
        _buildCardSettingsList_StatusKeluarga(),
        _buildCardSettingsList_Goldar(),
        _buildCardSettingsButton_Save(),
        _buildCardSettingsButton_Reset(),
      ],
    );
  }

  CardSettings _buildLandscapeLayout() {
    return CardSettings(
      labelPadding: 12.0,
      children: <Widget>[
        CardSettingsHeader(
          label: 'Person',
        ),
        _buildCardSettingsText_Name(),
        _buildCardSettingsText_Tlahir(),
        _buildCardSettingsDatePicker(),
        _buildCardSettingsText_Alamat(),
        _buildCardSettingsList_Jk(),
        _buildCardSettingsList_Agama(),
        _buildCardSettingsList_PendTerakhir(),
        _buildCardSettingsList_Pekerjaan(),
        _buildCardSettingsList_Status(),
        _buildCardSettingsList_StatusKeluarga(),
        _buildCardSettingsList_Goldar(),
        _buildCardSettingsButton_Save(),
        _buildCardSettingsButton_Reset(),
      ],
    );
  }

  //BUILDERS FOR EACH FIELD

  CardSettingsText _buildCardSettingsText_Name() {
    return CardSettingsText(
      enabled: false,
      key: _nameKey,
      label: 'Nama Lengkap',
      hintText: 'Masukkan nama lengkap',
      initialValue: _formModelKpnddkn.nama,
      autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Name type is required.';
        return null;
      },
      onSaved: (value) => _formModelKpnddkn.nama = value,
      onChanged: (value) => _showSnackBar('Name', value),
    );
  }

  CardSettingsText _buildCardSettingsText_Tlahir() {
    return CardSettingsText(
      visible: false,
      key: _lahirKey,
      label: 'Tempat Lahir',
      hintText: 'Masukkan tempat lahir',
      initialValue: _formModelKpnddkn.tempat_lahir,
      autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Name type is required.';
        return null;
      },
      onSaved: (value) => _formModelKpnddkn.tempat_lahir = value,
      onChanged: (value) => _showSnackBar('Name', value),
    );
  }

  CardSettingsDatePicker _buildCardSettingsDatePicker() {
    return CardSettingsDatePicker(
      key: _dateKey,
      icon: Icon(Icons.calendar_today),
      label: 'Tgl. Lahir',
      initialValue: _formModelKpnddkn.tgl_lahir,
      onSaved: (value) => _formModelKpnddkn.tgl_lahir =
          updateJustDate(value, _formModelKpnddkn.tgl_lahir),
      onChanged: (value) => _showSnackBar('Show Date', value),
    );
  }

  CardSettingsText _buildCardSettingsText_Alamat() {
    return CardSettingsText(
      key: _alamatKey,
      label: 'Alamat',
      hintText: 'Masukkan Alamat',
      initialValue: _formModelKpnddkn.nama,
      autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Alamat type is required.';
        return null;
      },
      onSaved: (value) => _formModelKpnddkn.nama = value,
      onChanged: (value) => _showSnackBar('Alamat', value),
    );
  }

  CardSettingsListPicker _buildCardSettingsList_Jk() {
    return CardSettingsListPicker(
      key: _jkKey,
      label: 'J. Kelamin',
      initialValue: _formModelKpnddkn.jk,
      hintText: 'Jenis Kelamin',
      autovalidate: _autoValid,
      options: <String>['Laki-Laki', 'Perempuan'],
      validator: (String value) {
        if (value == null || value.isEmpty) return 'You must pick a type.';
        return null;
      },
      onSaved: (value) => _formModelKpnddkn.jk = value,
      onChanged: (value) => _showSnackBar('Jenis Kelamin', value,)
    );
  }

  CardSettingsListPicker _buildCardSettingsList_Agama(){
    return CardSettingsListPicker(
        key: _agamaKey,
        label: 'Agama',
        initialValue: _formModelKpnddkn.agama,
        autovalidate: _autoValid,
        options: <String>['Islam','Kristen','Katholik','Hindu','Budha',],
        validator: (String value){
          if (value==null || value.isEmpty) return 'Harus di isi.';
        },
        onSaved: (value) => _formModelKpnddkn.agama = value,
        onChanged: (value) => _showSnackBar('Agama', value),
    );
  }

  CardSettingsListPicker _buildCardSettingsList_PendTerakhir(){
    return CardSettingsListPicker(
        key: _pendterakhirKey,
        label: 'Pend. Terkahir',
        initialValue: _formModelKpnddkn.pend_terakhir,
        autovalidate: _autoValid,
        options: <String>['SD/Sederajat','SMA/SMK Sederajat','SMP/Sederajat','S1/D4','D3','D1','S2','S3'],
        validator: (String value){
          if (value==null || value.isEmpty) return 'Harus di isi.';
        },
        onSaved: (value) => _formModelKpnddkn.pend_terakhir= value,
        onChanged: (value) => _showSnackBar('Pendidikan Terakhir', value),
    );
  }

   CardSettingsListPicker _buildCardSettingsList_Pekerjaan(){
    return CardSettingsListPicker(
        key: _pekerjaanKey,
        label: 'Pekerjaan',
        initialValue: _formModelKpnddkn.pekerjaan,
        autovalidate: _autoValid,
        options: <String>['Wiraswasta','PNS','Karyawan','Petani','Peternak','Pengusaha'],
        validator: (String value){
          if (value==null || value.isEmpty) return 'Harus di isi.';
        },
        onSaved: (value) => _formModelKpnddkn.pekerjaan = value,
        onChanged: (value) => _showSnackBar('Pekerjaan', value),
    );
  }

  CardSettingsListPicker _buildCardSettingsList_Status(){
    return CardSettingsListPicker(
        key: _statusKey,
        label: 'Status',
        initialValue: _formModelKpnddkn.status,
        autovalidate: _autoValid,
        options: <String>['Menikah','Lajang','Janda','Duda'],
        validator: (String value){
          if (value==null || value.isEmpty) return 'Harus di isi.';
        },
        onSaved: (value) => _formModelKpnddkn.status = value,
        onChanged: (value) => _showSnackBar('Status', value),
    );
  }

  CardSettingsListPicker _buildCardSettingsList_StatusKeluarga(){
    return CardSettingsListPicker(
        key: _statuskeluargaKey,
        label: 'Status Kel.',
        initialValue: _formModelKpnddkn.stts_keluarga,
        autovalidate: _autoValid,
        options: <String>['Anak','Ayah','Ibu','Paman','Bibi'],
        validator: (String value){
          if (value==null || value.isEmpty) return 'Harus di isi.';
        },
        onSaved: (value) => _formModelKpnddkn.stts_keluarga = value,
        onChanged: (value) => _showSnackBar('Status keluarga', value),
    );
  }

  CardSettingsListPicker _buildCardSettingsList_Goldar(){
    return CardSettingsListPicker(
        key: _goldarKey,
        label: 'Gol. Darah',
        initialValue: _formModelKpnddkn.gol_darah,
        autovalidate: _autoValid,
        options: <String>['O','A','B','AB','Tidak Tahu'],
        validator: (String value){
          if (value==null || value.isEmpty) return 'Harus di isi.';
        },
        onSaved: (value) => _formModelKpnddkn.gol_darah = value,
        onChanged: (value) => _showSnackBar('Gol. Darah', value),
    );
  }

   CardSettingsButton _buildCardSettingsButton_Reset() {
    return CardSettingsButton(
      label: 'RESET',
      onPressed: _resetPressed,
      backgroundColor: Colors.redAccent,
      textColor: Colors.white,
    );
  }

  CardSettingsButton _buildCardSettingsButton_Save() {
    return CardSettingsButton(
      label: 'SAVE',
      onPressed: _savePressed,
    );
  }

  /* EVENT HANDLERS */

  Future _savePressed() async {
    final form = _formKey.currentState;

    if (form.validate()) {
      form.save();
      showResultsKpnddukan(context, _formModelKpnddkn);
    } else {
      setState(() => _autoValid = true);
    }
  }

  void _resetPressed() {
    _formKey.currentState.reset();
  }

  void _showSnackBar(String label, dynamic value) {
    _scaffoldKey.currentState.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(label + ' = ' + value.toString()),
      ),
    );
  }
}

import 'package:card_settings/card_settings.dart';
import 'package:flutter/material.dart';
import 'model.dart';
import 'dart:async';

import 'result.dart';

class FormPembangunan extends StatefulWidget {
  static const String tag = 'form_pembangunan';
  @override
  _FormPembangunanState createState() => _FormPembangunanState();
}

class _FormPembangunanState extends State<FormPembangunan> {
  final _formModelPmbgnn = FormModelPembangunan();

  bool  _autoValid = false;

  final GlobalKey<FormState> _formPmbngnKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<FormState> _desaKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _tahunKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _bidangKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _subbidangKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _jkegiatanKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _lokkegiatanKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _sasaranKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _thnmulaiKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _thnberakhirKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _prakiraanbiayaKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _smbrbiayaKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _keteranganKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _pjpenyusunKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _indikasiKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _progbidangKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _praksasaranKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _ketindikasiKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: appBar(),
      body: Form(
        key: _formPmbngnKey,
        child: (orientation == Orientation.portrait)
            ? _buildPortraitLayout()
            : _buildLandscapeLayout(),
      ),
    );
  }

  AppBar appBar() => AppBar(
        title: Text("Form Pembangunan"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {},
          ),
        ],
        leading: IconButton(
          icon: Icon(Icons.refresh),
          onPressed: () {},
        ),
      );

  CardSettings _buildPortraitLayout() {
    return CardSettings(
      children: <Widget>[
        CardSettingsHeader(
          label: 'Rancangan RPJM Desa'),
        _buildCardSettingsTextDesa(),
        _buildCardSettingsTahun(),
        _buildCardSettingsTextBidang(),
        _buildCardSettingsTextSubbidang(),
        _buildCardSettingsTextJkegiatan(),
        _buildCardSettingsTextLokkegiatan(),
        _buildCardSettingsTextSasaran(),
        _buildCardSettingsThnmulai(),
        _buildCardSettingsThnberakhir(),
        _buildCardSettingsCurrencyBiaya(),  
        _buildCardSettingsTextSumberbiaya(),
        _buildCardSettingsParagrapKet(5),
        _buildCardSettingsTextPjpenyusun(),
        CardSettingsHeader(
          label: 'Indikasi RPJM Desa',
        ),
        _buildCardSettingsTextIndikasi(),
        _buildCardSettingsTextProgbidang(),
        _buildCardSettingsTextPrakSasaran(),
        _buildCardSettingsParagrapKetIndikasi(5),
        _buildCardSettingsButton_Save(),
        _buildCardSettingsButton_Reset()
      ],
    );
  }

  CardSettings _buildLandscapeLayout() {
    return CardSettings(
      labelPadding: 12.0,
      children: <Widget>[
        CardSettingsHeader(
          label: 'RPJM',
        ),
        _buildCardSettingsTextDesa(),
        _buildCardSettingsTahun(),
      ],
    );
  }

  

  CardSettingsText _buildCardSettingsTextDesa() {
    return CardSettingsText(
      key: _desaKey,
      label: 'Desa',
      hintText: 'Nama Desa',
      initialValue: _formModelPmbgnn.desa,
      autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Name type is required.';
        return null;
      },
      onSaved: (value) => _formModelPmbgnn.desa = value,
      onChanged: (value) => _showSnackBar('Desa', value),
    );
  }

  CardSettingsDatePicker _buildCardSettingsTahun() {
    return CardSettingsDatePicker(
      key: _tahunKey,
      icon: Icon(
        Icons.calendar_today,
      ),
      label: 'Tahun',
      initialValue: _formModelPmbgnn.tahun,
      onSaved: (value) => _formModelPmbgnn.tahun =
          updateJustDate(value, _formModelPmbgnn.tahun),
      onChanged: (value) => _showSnackBar('Tahun', value),
    );
  }

  CardSettingsText _buildCardSettingsTextBidang() {
    return CardSettingsText(
      key: _bidangKey,
      label: 'Bidang',
      hintText: 'Masukkan Bidang',
      initialValue: _formModelPmbgnn.bidang,
      autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Name type is required.';
        return null;
      },
      onSaved: (value) => _formModelPmbgnn.bidang = value,
      onChanged: (value) => _showSnackBar('Bidang', value),
    );
  }

  CardSettingsText _buildCardSettingsTextSubbidang() {
    return CardSettingsText(
      key: _subbidangKey,
      label: 'Sub Bidang',
      hintText: 'Masukkan Sub Bidang',
      initialValue: _formModelPmbgnn.sub_bidang,
      autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Name type is required.';
        return null;
      },
      onSaved: (value) => _formModelPmbgnn.sub_bidang = value,
      onChanged: (value) => _showSnackBar('Sub Bidang', value),
    );
  }

  CardSettingsText _buildCardSettingsTextJkegiatan() {
    return CardSettingsText(
      key: _jkegiatanKey,
      label: 'Jenis Kegiatan',
      hintText: 'Masukkan Jenis kegiatan',
      initialValue: _formModelPmbgnn.j_kegiatan,
      autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Name type is required.';
        return null;
      },
      onSaved: (value) => _formModelPmbgnn.j_kegiatan = value,
      onChanged: (value) => _showSnackBar('Jenis Kegiatan', value),
    );
  }

  CardSettingsText _buildCardSettingsTextLokkegiatan() {
    return CardSettingsText(
      key: _lokkegiatanKey,
      label: 'Lokasi',
      hintText: 'Masukkan Lokasi',
      initialValue: _formModelPmbgnn.lok_kegiatan,
      autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Name type is required.';
        return null;
      },
      onSaved: (value) => _formModelPmbgnn.sub_bidang = value,
      onChanged: (value) => _showSnackBar('Lokasi', value),
    );
  }

  CardSettingsText _buildCardSettingsTextSasaran() {
    return CardSettingsText(
      key: _sasaranKey,
      label: 'Sasaran',
      hintText: 'Masukkan Sasaran',
      initialValue: _formModelPmbgnn.sasaran,
      autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Name type is required.';
        return null;
      },
      onSaved: (value) => _formModelPmbgnn.sasaran = value,
      onChanged: (value) => _showSnackBar('Sasaran', value),
    );
  }

  CardSettingsDatePicker _buildCardSettingsThnmulai() {
    return CardSettingsDatePicker(
      key: _thnmulaiKey,
      icon: Icon(
        Icons.calendar_today,
      ),
      label: 'Tahun',
      initialValue: _formModelPmbgnn.tahun_mulai,
      onSaved: (value) => _formModelPmbgnn.tahun_mulai =
          updateJustDate(value, _formModelPmbgnn.tahun_mulai),
      onChanged: (value) => _showSnackBar('Tahun Mulai', value),
    );
  }

  CardSettingsDatePicker _buildCardSettingsThnberakhir() {
    return CardSettingsDatePicker(
      key: _thnberakhirKey,
      icon: Icon(
        Icons.calendar_today,
      ),
      label: 'Tahun',
      initialValue: _formModelPmbgnn.tahun_mulai,
      onSaved: (value) => _formModelPmbgnn.tahun_berakhir =
          updateJustDate(value, _formModelPmbgnn.tahun_berakhir),
      onChanged: (value) => _showSnackBar('Tahun Berakhir', value),
    );
  }

  CardSettingsCurrency _buildCardSettingsCurrencyBiaya() {
    return CardSettingsCurrency(
      key: _prakiraanbiayaKey,
      label: 'Prakiraan Biaya',
      initialValue: _formModelPmbgnn.prakiraan_biaya,
      autovalidate: _autoValid,
      validator: (value) {
        if (value != null && value > 100) return 'No scalpers allowed!';
        return null;
      },
      onSaved: (value) => _formModelPmbgnn.prakiraan_biaya = value,
      onChanged: (value) => _showSnackBar('Prikiraan Biaya', value),
    );
  }

  CardSettingsText _buildCardSettingsTextSumberbiaya() {
    return CardSettingsText(
      key: _smbrbiayaKey,
      label: 'SumberBiaya',
      hintText: 'Masukkan Sumber Biaya',
      initialValue: _formModelPmbgnn.sumber_biaya,
      autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Name type is required.';
        return null;
      },
      onSaved: (value) => _formModelPmbgnn.sumber_biaya = value,
      onChanged: (value) => _showSnackBar('Sasaran', value),
    );
  }

  CardSettingsParagraph _buildCardSettingsParagrapKet(int lines) {
    return CardSettingsParagraph(
      key: _keteranganKey,
      label: 'Keterangan',
      initialValue: _formModelPmbgnn.keterangan,
      numberOfLines: lines,
      onSaved: (value) => _formModelPmbgnn.keterangan = value,
      onChanged: (value) => _showSnackBar('Keterangan', value),
    );
  }

  CardSettingsText _buildCardSettingsTextPjpenyusun() {
    return CardSettingsText(
      key: _pjpenyusunKey,
      label: 'PJ Penyusun',
      hintText: 'Masukkan Nama Penyusun',
      initialValue: _formModelPmbgnn.pj_penyusun,
      autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Name type is required.';
        return null;
      },
      onSaved: (value) => _formModelPmbgnn.pj_penyusun = value,
      onChanged: (value) => _showSnackBar('PJ Penyusun', value),
    );
  }

  //Indikasi RPJM Desa
  CardSettingsText _buildCardSettingsTextIndikasi() {
    return CardSettingsText(
      key: _indikasiKey,
      label: 'Indikasi',
      hintText: 'Masukkan Nama Indikasi',
      initialValue: _formModelPmbgnn.indikasi,
      autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Name type is required.';
        return null;
      },
      onSaved: (value) => _formModelPmbgnn.indikasi = value,
      onChanged: (value) => _showSnackBar('Indikasi', value),
    );
  }

  CardSettingsText _buildCardSettingsTextProgbidang() {
    return CardSettingsText(
      key: _progbidangKey,
      label: 'Prog. Bidang',
      hintText: 'Masukkan Bidang',
      initialValue: _formModelPmbgnn.program_bidang,
      autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Name type is required.';
        return null;
      },
      onSaved: (value) => _formModelPmbgnn.program_bidang = value,
      onChanged: (value) => _showSnackBar('Prog. Bidang', value),
    );
  }

  CardSettingsText _buildCardSettingsTextPrakSasaran() {
    return CardSettingsText(
      key: _praksasaranKey,
      label: 'Sasaran',
      hintText: 'Masukkan Prakiraan',
      initialValue: _formModelPmbgnn.prakiraan_sasaran,
      autovalidate: _autoValid,
      validator: (value) {
        if (value == null || value.isEmpty) return 'Name type is required.';
        return null;
      },
      onSaved: (value) => _formModelPmbgnn.prakiraan_sasaran = value,
      onChanged: (value) => _showSnackBar('Prakiraan', value),
    );
  }

  CardSettingsParagraph _buildCardSettingsParagrapKetIndikasi(int lines) {
    return CardSettingsParagraph(
      key: _ketindikasiKey,
      label: 'Keterangan',
      initialValue: _formModelPmbgnn.keterangan_indikasi,
      numberOfLines: lines,
      onSaved: (value) => _formModelPmbgnn.keterangan_indikasi = value,
      onChanged: (value) => _showSnackBar('Keterangan', value),
    );
  }

  CardSettingsButton _buildCardSettingsButton_Reset() {
    return CardSettingsButton(
      label: 'RESET',
      onPressed: _resetPressed,
      backgroundColor: Colors.redAccent,
      textColor: Colors.white,
    );
  }

  CardSettingsButton _buildCardSettingsButton_Save() {
    return CardSettingsButton(
      label: 'SAVE',
      onPressed: _savePressed,
    );
  }


  Future _savePressed() async {
    final form = _formPmbngnKey.currentState;

    if (form.validate()) {
      form.save();
      showResultsPmbngn(context, _formModelPmbgnn);
    } else {
      setState(() => _autoValid = true);
    }
  }

  void _resetPressed() {
    _formPmbngnKey.currentState.reset();
  }

  void _showSnackBar(String label, dynamic value) {
    _scaffoldKey.currentState.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(label + ' = ' + value.toString()),
      ),
    );
  }
}

import 'package:flutter/material.dart';

import 'home.dart';
import 'splash.dart';
import 'login.dart';
//FORM 
import 'destinations/info_desa.dart';
import 'destinations/form_penduduk.dart';
import 'destinations/map_screen.dart';
import 'destinations/form_pembangunan.dart';

// import 'package:map_view/map_view.dart';

// var API_KEY = 'AIzaSyDTc9pQG8Dn5vuD1IxKbmzC2ZxV7QT28u4';

void main() {
  // MapView.setApiKey('AIzaSyDTc9pQG8Dn5vuD1IxKbmzC2ZxV7QT28u4');
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    Splash.tag: (context) => Splash(),
    Login.tag: (context) => Login(),
    Infodesa.tag: (context) => Infodesa(),
    FormPenduduk.tag: (context) => FormPenduduk(),
    FormPembangunan.tag: (context) => FormPembangunan(),
    MapScreen.tag: (BuildContext context) => MapScreen(),
    HomeActivity.tag: (BuildContext context) => new HomeActivity(),
  };
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        accentColor: Colors.yellow[600], // background color of card headers
        cardColor: Colors.grey[200], // background color of fields
        backgroundColor: Colors.yellow[200], // color outside the card
        primaryColor: Colors.yellow[600], // color of page header
        buttonColor: Colors.yellow[600], // background color of buttons
        textTheme: TextTheme(
          button:
              TextStyle(color: Colors.black), // style of button text
          subhead: TextStyle(color: Colors.grey[800]), // style of input text
        ),
        primaryTextTheme: TextTheme(
          title: TextStyle(color: Colors.black), // style for headers
        ),
        inputDecorationTheme: InputDecorationTheme(
          labelStyle: TextStyle(color: Colors.black), // style for labels
        ),
      ),
      home: new Login(),
      routes: routes,
    );
  }
}
